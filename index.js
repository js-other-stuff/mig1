'use strict';
Object.defineProperty(global, 'Promise', {
    configurable: false,
    writable: false,
    value: require('bluebird')
});

const config  = require('bi-config');
const Service = require('bi-service').Service;


const service = module.exports = new Service(config);

service.on('set-up', function() {
    require('./lib/app.js');
});


const sdkErrors = {
    400: Service.error.RequestError,
    401: Service.error.UnauthorizedError,
    404: Service.error.ServiceError,
    500: Service.error.ServiceError
};

/**
 * service resources eg. storage connections, remote service SDKs
 * each registered resource have to implement `inspectIntegrity` method
 */
const resourceMgr = service.resourceManager;
/**
 * connect to and talk with other bi-service based services
 */
const remoteServiceMgr = service.getRemoteServiceManager();


//Instantiates and registers a BIServiceSDK
//requires relavant npm SDK package to be installed
//remoteServiceMgr.buildRemoteService('<service>:<app>:<api_version>', {
    //errors: sdkErrors,
    //axios options supported
//});

// bi-service plugins
//Service monitoring/inspection
require('bi-service-cli');
//Autogeneration of Service API documentation
require('bi-service-doc');
