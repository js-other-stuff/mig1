'use strict';
const moduleLoader = require('bi-service').moduleLoader;
const service      = require('../index.js');

service.buildApp('public', {
    validator: {schemas: {}}
});


exports.appManager = service.appManager;

moduleLoader.loadModules([
    __dirname + '/routes/v1.0/'
], {
    except: []
});
