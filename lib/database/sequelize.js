'use strict';
const config           = require('bi-config');
const process          = require('process');
const path             = require('path');
const Service          = require('bi-service');
const sequelizeBuilder = require('bi-service-sequelize');
const debug            = require('debug')('sequelize');

var _ = require('lodash');

var options     = _.cloneDeep(config.get('apps:public:storage:sequelize'));
options.logging = true;
let sequelize   = sequelizeBuilder(options);

//sequelize.loadModels(path.join(process.cwd(), '/lib/models/orm')); will work after dave merge https://webdev-git.czupc.bistudio.com/fogine/bi-service-sequelize/issues/2
Service.moduleLoader.fileIterator([path.join(process.cwd(), '/lib/models/orm')], {}, function(file, dir) {
    var validExtension = '.js';
    //if there is node >= 6.11 on server this can be rplaced with .endsWith
    if (file.indexOf(validExtension) !== file.length - validExtension.length) {
        return;
    }
    var pth = path.join(dir, file);
    debug('Importing file ', pth);
    sequelize.import(pth);
});

Object.keys(sequelize.models).forEach(function(modelName) {
    if ("associate" in sequelize.models[modelName]) {
        sequelize.models[modelName].associate(sequelize.models);
    }
});

module.exports = sequelize;
