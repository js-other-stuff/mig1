'use strict';

module.exports = (sequelize, DataTypes) => {

    const Op = sequelize.Op;

    const T = sequelize.define('Table1',
        {
            id      : {
                type        : DataTypes.INTEGER,
                primaryKey  : true
            },
            value: {
                type        : DataTypes.UUIDV4,
            }
        },
        {
            freezeTableName: true,
            tableName      : 'table1',
            timestamps     : false,
            underscored    : false
        }
    );

    return T;
};
